def sig_hook(hub, exc_type, exc_value, exc_traceback):
    """
    Handle an exception by displaying it with a traceback on sys.stderr
    """
