import json
import subprocess
import sys


def test_hook(runpy):
    ret = subprocess.run(
        [sys.executable, str(runpy)], env={}, capture_output=True, encoding="utf-8"
    )
    assert ret.returncode
    assert "Exception: Text exception" in ret.stderr
    output = json.loads(ret.stdout)
    assert "except.hook.default" == output["ref"]
