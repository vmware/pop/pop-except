import json
import subprocess
import sys

import pytest


def test_pop_exception(hub):
    with pytest.raises(Exception) as e:
        raise hub.exc.init.PopException("Error text")

    assert "Error text" in str(e)


# "hook_plugin": {"os": "POP_EXCEPTION_HOOK_PLUGIN", "help": "The exception hook plugin to use", "type": str,


def test_conf_traceback(runpy):
    ret = subprocess.run(
        [sys.executable, str(runpy)],
        env={"POP_TRACEBACK_LIMIT": "0"},
        capture_output=True,
        encoding="utf-8",
    )
    assert "Exception: Text exception" in ret.stderr
    output = json.loads(ret.stdout)
    assert "except.hook.default" == output["ref"]
