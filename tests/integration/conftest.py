import pathlib

import pytest


@pytest.fixture(scope="function", name="hub")
def integration_hub(hub):
    # Everytime we call this it will reset the hook to the default
    hub.pop.sub.add("pop_except")
    yield hub


@pytest.fixture
def runpy():
    return pathlib.Path(__file__).parent.parent / "run.py"
