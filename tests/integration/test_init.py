def test_except_aliases(hub):
    assert hub.exc.init.PopException
    assert hub.except_.init.PopException
    assert hub["except"].init.PopException


def test_hook_aliases(hub):
    assert hub.exc.hook.default.hook
    assert hub.except_.hook.default.hook
    assert hub["except"].hook.default.hook
