import json
import sys

import pop.hub


if __name__ == "__main__":
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_except")
    ref = getattr(sys.excepthook, "ref", None)
    print(json.dumps({"ref": ref, "OPT": dict(hub.OPT.pop_except)}))
    raise Exception("Text exception")
